# -*- coding:utf-8 -*-

import sys
import re

CN_NUM = {

u'0' : 0,
u'1' : 1,
u'2' : 2,
u'3' : 3,
u'4' : 4,
u'5' : 5,
u'6' : 6,
u'7' : 7,
u'8' : 8,
u'9' : 9,
}

CN_UNIT = {
u'十' : 10,
u'拾' : 10,
u'百' : 100,
u'佰' : 100,
u'千' : 1000,
u'仟' : 1000,
u'万' : 10000,
u'萬' : 10000,
u'亿' : 100000000,
u'億' : 100000000,
u'兆' : 1000000000000,
}


def str2number(numberString):

    numberString = numberString.strip()

    lcn = list(numberString)
    unit = 0 #当前的单位
    ldig = []#临时数组

    while lcn:
        cndig = lcn.pop()

        if CN_UNIT.has_key(cndig):
            unit = CN_UNIT.get(cndig)
            if unit==10000:
                ldig.append('w')    #标示万位
                unit = 1
            elif unit==100000000:
                ldig.append('y')    #标示亿位
                unit = 1
            elif unit==1000000000000:#标示兆位
                ldig.append('z')
                unit = 1

            continue

        elif CN_NUM.has_key(cndig):
            dig = CN_NUM.get(cndig)

            if unit:
                dig = dig*unit
                unit = 0

            ldig.append(dig)
        else:
            ldig.append(cndig)

    if unit==10:    #处理10-19的数字
        ldig.append(10)

    ret = 0
    tmp = 0
    dotflag = 0
    dotnumber = 1

    while ldig:
        x = ldig.pop()

        if x=='w':
            tmp *= 10000
            ret += tmp
            tmp=0

        elif x=='y':
            tmp *= 100000000
            ret += tmp
            tmp=0

        elif x=='z':
            tmp *= 1000000000000
            ret += tmp
            tmp=0

        elif x == '.':
            dotflag = 1
        else:
            if dotflag:
                dotnumber = dotnumber * 10.0
                x = x / dotnumber
            else:
                tmp = tmp * 10

            tmp += x

    ret += tmp
    return ret

    #ldig.reverse()
    #print ldig
    #print CN_NUM[u'七']


if __name__ == '__main__':
    test_dig = [u'51.80万', u'12.8', u'100万', u'176亿']

    for cn in test_dig:
        print str2number(cn)

    result = re.findall(r"\d+\.?\d*",u'xxxx万')
    if result:
        print result[0]
