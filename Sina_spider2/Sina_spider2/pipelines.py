# -*- coding: utf-8 -*-
import pymongo
from items import InformationItem, TweetsItem, TopicItem


class MongoDBPipleline(object):
    def __init__(self):
        clinet = pymongo.MongoClient("124.250.36.179", 27017)
        db = clinet["auto_weibo"]
        self.Information = db["Information"]
        self.Tweets = db["Tweets"]
        self.Follows = db["Follows"]
        self.Fans = db["Fans"]
        self.Topic = db["Topic"]

    def process_item(self, item, spider):
        """ 判断item的类型，并作相应的处理，再入数据库 """
        if isinstance(item, InformationItem):
            try:
                self.Information.insert(dict(item))
            except Exception:
                pass
        elif isinstance(item, TweetsItem):
            try:
                self.Tweets.insert(dict(item))
            except Exception:
                pass
        elif isinstance(item, TopicItem):
            try:
                self.Topic.replace_one({'homepage':item.get('homepage')}, dict(item), True)
            except Exception as e:
                print e.message
                pass
        return item
